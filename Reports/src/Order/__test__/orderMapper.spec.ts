import { OrderMapper } from '../Service/OrderMapper';
import { Repository } from '../Service/Repository';
import { Test } from '@nestjs/testing';
import { InternalServerErrorException } from '@nestjs/common';

describe('OrderMapper', () => {
    let orderMapper: OrderMapper;
    let repository: Repository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [Repository, OrderMapper],
            exports: [OrderMapper],
        }).compile();

        orderMapper = module.get<OrderMapper>(OrderMapper);
        repository = module.get<Repository>(Repository);
    });
    describe('Best buyers', () => {
        it('should find proper products given correct date', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }];
            const orders = [{ number: '2019/07/1', customer: 1, createdAt: date, products: [1] }];
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestBuyersByDate({ date: new Date(date) });
            expect(res).toEqual([{ customerName: 'John Doe', totalPrice: 1337 }]);
        });

        it('should run properly for 2 users when one of them got more than 1 order', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }, { id: 11, name: 'Testing hat', price: 13370 }];
            const orders = [
                { number: '2019/07/1', customer: 1, createdAt: date, products: [1] },
                { number: '2019/07/2', customer: 2, createdAt: date, products: [1] },
                { number: '2019/07/3', customer: 1, createdAt: date, products: [11] },
            ];
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }, { id: 2, firstName: 'Jan', lastName: 'Kowalski' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestBuyersByDate({ date: new Date(date) });
            expect(res).toEqual([{ customerName: 'John Doe', totalPrice: 14707 }, { customerName: 'Jan Kowalski', totalPrice: 1337 }]);
        });

        it('should throw if product doesnt exist', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }];
            const orders = [
                { number: '2019/07/1', customer: 1, createdAt: date, products: [1] },
                { number: '2019/07/2', customer: 2, createdAt: date, products: [2] },
                { number: '2019/07/3', customer: 1, createdAt: date, products: [1] },
            ];
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }, { id: 2, firstName: 'Jan', lastName: 'Kowalski' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            await expect(orderMapper.getBestBuyersByDate({ date: new Date(date) })).rejects.toThrowError(InternalServerErrorException);
        });

        it('should return empty array if no data available', async () => {
            const date = '2019-08-07';
            const products = [];
            const orders = [];
            const customers = [];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestBuyersByDate({ date: new Date(date) });
            expect(res).toEqual([]);
        });
    });

    describe('Best sellers', () => {
        it('should find proper products given correct date', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }];
            const orders = [{ number: '2019/07/1', customer: 1, createdAt: date, products: [1] }];
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestProductsByDate({ date: new Date(date) });
            expect(res).toEqual([{ productName: 'Testing shoes', quantity: 1, totalPrice: 1337 }]);
        });

        it('should match quantity with number of orders', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }];
            const orders = Array(100).fill({ number: '2019/07/1', customer: 1, createdAt: date, products: [1] });
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestProductsByDate({ date: new Date(date) });
            expect(res).toEqual([{ productName: 'Testing shoes', quantity: 100, totalPrice: 133700 }]);
        });

        it('should throw if product doesnt exist', async () => {
            const date = '2019-08-07';
            const products = [{ id: 1, name: 'Testing shoes', price: 1337 }];
            const orders = [
                { number: '2019/07/1', customer: 1, createdAt: date, products: [1] },
                { number: '2019/07/2', customer: 2, createdAt: date, products: [2] },
                { number: '2019/07/3', customer: 1, createdAt: date, products: [1] },
            ];
            const customers = [{ id: 1, firstName: 'John', lastName: 'Doe' }, { id: 2, firstName: 'Jan', lastName: 'Kowalski' }];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            await expect(orderMapper.getBestProductsByDate({ date: new Date(date) })).rejects.toThrowError(InternalServerErrorException);
        });

        it('should return empty array if no data available', async () => {
            const date = '2019-08-07';
            const products = [];
            const orders = [];
            const customers = [];
            // mock repo
            jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(products));
            jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(customers));
            jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(orders));
            // run test
            const res = await orderMapper.getBestProductsByDate({ date: new Date(date) });
            expect(res).toEqual([]);
        });
    });
});
