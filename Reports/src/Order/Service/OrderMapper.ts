import { Injectable, Inject, InternalServerErrorException } from '@nestjs/common';
import { Repository } from './Repository';
import { DailyStatsDto } from '../../Report/Dto/DailyStatsDto';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { IBestSellers } from 'src/Report/Model/IReports';
import { Customer } from '../Model/Customer';
import { IBestBuyers } from '../../Report/Model/IReports';

// would probably fit into Order definitions
interface BestSellers {
  [key: string]: IBestSellers;
}

interface BestBuyers {
  [key: string]: IBestBuyers;
}

interface ProductsById {
  [key: string]: Product;
}

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;
  async getBestProductsByDate({ date }: DailyStatsDto): Promise<IBestSellers[]> {
    const orders = await this.repository.fetchOrders();
    const products = await this.repository.fetchProducts();

    const dailyOrders = orders.filter(order => sameDay(new Date(order.createdAt), date));

    const bestSellersObject = this.prepareProductsAccumulator(products);
    const productsById = this.castProductsToObject(products);

    const bestSellersTemp = this.countBestSellers(bestSellersObject, dailyOrders, productsById);
    const bestSellers = Object.values(bestSellersTemp) // lose the object structure
      .filter(x => x.totalPrice) // don't return products with totalPrice = 0
      .sort((a, b) => b.totalPrice - a.totalPrice); // sort by price
    return bestSellers;
  }

  async getBestBuyersByDate({ date }: DailyStatsDto): Promise<IBestBuyers[]> {
    const orders = await this.repository.fetchOrders();
    const products = await this.repository.fetchProducts();
    const customers = await this.repository.fetchCustomers();

    const dailyOrders = orders.filter(order => sameDay(new Date(order.createdAt), date));

    const bestBuyersObject = this.prepareBuyersAccumulator(customers);
    const productsById = this.castProductsToObject(products);

    const bestBuyersTemp = this.countBestBuyers(bestBuyersObject, dailyOrders, productsById);
    const bestBuyers = Object.values(bestBuyersTemp) // lose the object struxture
      .filter(x => x.totalPrice) // don't return buyers with totalPrice = 0
      .sort((a, b) => b.totalPrice - a.totalPrice); // sort by price
    return bestBuyers;
  }

  private prepareProductsAccumulator(products: Product[]): BestSellers {
    return products.reduce((acc, product) => {
      acc[product.id] = {
        productName: product.name,
        quantity: 0,
        totalPrice: 0,
      };
      return acc;
    }, {});
  }

  private prepareBuyersAccumulator(customers: Customer[]) {
    return customers.reduce((acc, customer) => {
      acc[customer.id] = {
        customerName: `${customer.firstName} ${customer.lastName}`,
        totalPrice: 0,
      };
      return acc;
    }, {});
  }

  private castProductsToObject(elements: Product[]): ProductsById {
    return elements.reduce((acc, element) => {
      acc[element.id] = element;
      return acc;
    }, {});
  }

  private countBestSellers(bestSellers: BestSellers, orders: Order[], products: ProductsById) {
    orders.forEach(order => {
      order.products.forEach(id => {
        const product = products[id];
        if (!product) {
          throw new InternalServerErrorException();
        }
        bestSellers[id].quantity++;
        bestSellers[id].totalPrice = Math.round((bestSellers[order.customer].totalPrice + product.price) * 100) / 100; // <3 js
      });
    });
    return bestSellers;
  }

  private countBestBuyers(bestBuyers: BestBuyers, orders: Order[], products: ProductsById) {
    orders.forEach(order => {
      order.products.forEach(id => {
        const product = products[id];
        if (!product) {
          throw new InternalServerErrorException();
        }
        bestBuyers[order.customer].totalPrice = Math.round((bestBuyers[order.customer].totalPrice + product.price) * 100) / 100; // js <3
      });
    });
    return bestBuyers;
  }
}

// https://stackoverflow.com/questions/43855166/how-to-tell-if-two-dates-are-in-the-same-day
function sameDay(d1: Date, d2: Date): boolean {
  return d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate();
}
