import { Controller, Get, Param } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { DailyStatsDto } from '../Dto/DailyStatsDto';
import { DatePipe } from '../Pipes/DatePipe';
import { IBestSellers, IBestBuyers } from '../Model/IReports';

@Controller('/report')
export class ReportController {
  constructor(
    private orderMapper: OrderMapper,
  ) {}

  @Get('/products/:date')
  bestSellers(@Param(DatePipe) dto: DailyStatsDto): Promise<IBestSellers[]> {
    return this.orderMapper.getBestProductsByDate(dto);
  }

  @Get('/customer/:date')
  async bestBuyers(@Param(DatePipe) dto: DailyStatsDto): Promise<IBestBuyers[]> {
    return this.orderMapper.getBestBuyersByDate(dto);
  }
}
