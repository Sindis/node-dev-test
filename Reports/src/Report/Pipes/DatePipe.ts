import { PipeTransform, BadRequestException } from '@nestjs/common';
import { DailyStatsDto } from '../Dto/DailyStatsDto';

// that didn't have to be pipe and dto but I had different concept at first
// @IsDateString from 'class-validator' would be enough
export class DatePipe implements PipeTransform {
    transform(value: any): Promise<DailyStatsDto> {
        const date = new Date(value.date);
        if (!(date instanceof Date) || isNaN(date.valueOf())) {
            throw new BadRequestException(`"${value.date}" is not a valid date`);
        }
        return Promise.resolve({ date: new Date(date) });
    }
}
