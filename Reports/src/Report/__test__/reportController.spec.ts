
import { Test } from '@nestjs/testing';
import { ReportController } from '../Controller/ReportController';
import { OrderModule } from '../../Order/OrderModule';
import { Repository } from '../../Order/Service/Repository';

describe('OrderMapper', () => {
    let reportController: ReportController;
    const repository: Repository = {
        fetchOrders: () => Promise.resolve(require('../../Order/Resources/Data/orders')),
        fetchProducts: () => Promise.resolve(require('../../Order/Resources/Data/products')),
        fetchCustomers: () => Promise.resolve(require('../../Order/Resources/Data/customers')),
    };

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [OrderModule],
            controllers: [ReportController],
            providers: [],
        })
        .overrideProvider(Repository)
        .useValue(repository)
        .compile();

        reportController = module.get<ReportController>(ReportController);
    });

    it('should trigger relevant service for bestBuyers and return value from whole flow', async () => {
        const res = await reportController.bestBuyers({ date: new Date('2019-08-07') });
        // here we should mock repository pretty much in the same way as it's done in e2e
        expect(res).toEqual([ {
            customerName: 'John Doe',
            totalPrice: 135.75,
        }, {
            customerName: 'Jane Doe',
            totalPrice: 110,
        }]);
    });

    it('should trigger relevant service for bestSellers and return value from whole flow', async () => {
        const res = await reportController.bestSellers({ date: new Date('2019-08-07') });
        // here we should mock repository pretty much in the same way as it's done in orderMapper
        expect(res).toEqual([{
            productName: 'Black sport shoes',
            quantity: 2,
            totalPrice: 245.75,
        },
        {
            productName: 'Cotton t-shirt XL',
            quantity: 1,
            totalPrice: 135.75,
        }]);
    });
});
