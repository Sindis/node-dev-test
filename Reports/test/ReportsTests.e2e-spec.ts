import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';
import { Repository } from '../src/Order/Service/Repository';

describe('', () => {
  let app: INestApplication;
  const repository: Repository = {
    fetchOrders: () => Promise.resolve([]),
    fetchProducts: () => Promise.resolve([]),
    fetchCustomers: () => Promise.resolve([]),
  };

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    })
    .overrideProvider(Repository)
    .useValue(repository)
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });
  describe('/report/products', () => {
    it('should return empty array when no data', () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve([]));
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve([]));
      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve([]));

      return request(app.getHttpServer())
        .get('/report/products/2019-08-8')
        .expect(200)
        .expect('[]');
    });

    it('should return empty array when wrong date', () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

      return request(app.getHttpServer())
        .get('/report/products/2029-08-8')
        .expect(200)
        .expect('[]');
    });

    it('should return proper response with proper date', () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

      return request(app.getHttpServer())
        .get('/report/products/2019-08-8')
        .expect(200)
        .expect(
          JSON.stringify([{
            productName: 'Blue jeans',
            quantity: 1,
            totalPrice: 165.99,
          },
          {
            productName: 'Cotton t-shirt XL',
            quantity: 1,
            totalPrice: 135.75 },
          {
            productName: 'Black sport shoes',
            quantity: 1,
            totalPrice: 110,
          }]),
        );
    });

    it('should throw if broken date', () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

      return request(app.getHttpServer())
        .get('/report/products/nestjs')
        .expect(400);
    });

    it('should throw if referenced product doesnt exist', () => {
      jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
      jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve([]));
      jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

      return request(app.getHttpServer())
        .get('/report/products/2019-08-08')
        .expect(500);
    });
  });

  describe('/reports/customer', () => {
      it('should return empty array when no data', () => {
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve([]));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve([]));
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve([]));

        return request(app.getHttpServer())
          .get('/report/customer/2019-08-8')
          .expect(200)
          .expect('[]');
      });

      it('should return empty array when wrong date', () => {
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

        return request(app.getHttpServer())
          .get('/report/customer/2029-08-8')
          .expect(200)
          .expect('[]');
      });

      it('should return proper response with proper date', () => {
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

        return request(app.getHttpServer())
          .get('/report/customer/2019-08-8')
          .expect(200)
          .expect(
            JSON.stringify([
              { customerName: 'Jane Doe', totalPrice: 110 },
              { customerName: 'John Doe', totalPrice: 81.74 },
            ]),
          );
      });

      it('should throw if broken date', () => {
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/products')));
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

        return request(app.getHttpServer())
          .get('/report/customer/nestjs')
          .expect(400);
      });

      it('should throw if referenced product doesnt exist', () => {
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/orders')));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => Promise.resolve([]));
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => Promise.resolve(require('../src/Order/Resources/Data/customers')));

        return request(app.getHttpServer())
          .get('/report/customer/2019-08-08')
          .expect(500);
      });
  });
});
