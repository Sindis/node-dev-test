import { SortInterface } from 'Model/ISort';

export class ASort implements SortInterface {
    constructor(
        private array: number[]
    ) {}

    sort() {
        let triggered = true;
        while(triggered) {
            triggered = false;
            this.array.forEach((_, i, arr) => { // this.array.sort((a, b) => a - b)
                if(arr[i] > arr[i + 1]) {
                    triggered = true;
                    [arr[i + 1], arr[i]] = [arr[i], arr[i + 1]]; // no need for temp variables this way
                }
            })
        }
        return this;
    }

    get result() {
        return this.array;
    }
}