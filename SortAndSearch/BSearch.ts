export class BSearch {
    static find(value: number, array: number[]): BSearchInstance {
        const instance = new BSearchInstance(value, array);
        return instance.find();
    }
}

class BSearchInstance {
    private count = 0;
    constructor(
        private value: number,
        private array: number[],
    ) {}

    find(): this {
        const arr = this.array; // just to shorten the code
        let firstIndex = 0;
        let lastIndex = this.array.length - 1;
        while (firstIndex <= lastIndex) {
            this.count++;
            const middle = Math.floor((firstIndex + lastIndex) / 2);
            if (arr[middle] === this.value) {
                this.value = middle; // found it
                return this;
            }
            if (arr[middle] < this.value) {
                firstIndex = middle + 1; // +1 since middle doesn't fit
            } else {
                lastIndex = middle - 1; // -1  since middle doesn't fit
            }
        }
        this.value = -1; // not found, return -1
        return this;
    }

    get operations(): number {
        return this.count;
    }

    get result() {
        return this.value;
    }
}

// or, for singleton
// export default new BSearch();
// usage => bSearch.find(array, value);