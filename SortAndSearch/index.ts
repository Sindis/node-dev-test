import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const bubbleSort = new ASort(unsorted).sort().result;
console.log(bubbleSort);

console.log(elementsToFind.map((x) => BSearch.find(x, bubbleSort).result)); // or .operations

const insertionSort = new BSort(unsorted).sort().result;
console.log(insertionSort);

console.log(elementsToFind.map((x) => BSearch.find(x, insertionSort).operations)); // or .result
