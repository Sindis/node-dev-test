import { SortInterface } from './Model/ISort';

export class BSort implements SortInterface{
    constructor(
        private array: number[]
    ) {}

    sort() {
        this.array.forEach((_, i, arr) => {
            if(!i) return;
            if(arr[i] < arr[0]) { // move to front
                const element = arr.splice(i, 1)[0];
                arr = [element, ...arr];
            } else if (arr[i] > arr[i - 1]) { // order is fine, skip
                return;
            } else {
                arr.forEach((_, j) => {
                    if (arr[i] > arr[j - 1] && arr[i] < arr[j]) { // find the lowest place it could possibly go
                        const element = arr.splice(i, 1)[0];
                        arr.splice(j, 0, element)
                    }
                })
            }
        })
        return this;
    }

    get result() {
        return this.array;
    }
}