export interface SortInterface {
    sort(array: number[]): this;
    readonly result: number[];
}